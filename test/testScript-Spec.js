import chai from 'chai';
import { expect } from 'chai';
import chaiExclude from 'chai-exclude';

chai.use(chaiExclude);
const actualMenu = require('../pos1-menu.json');
const expectedMenu = require('../stag-menu.json');

describe('compare_menu_documents', () => {
  const expectedMenuCategories = expectedMenu.categories.category;
  const actualMenuCategories = actualMenu.categories.category;

  const expectedMenuAttributeSets = expectedMenu.attributeSets.attributeSet;
  const actualMenuAttributeSets = actualMenu.attributeSets.attributeSet;

  const expectedMenuTaxCategories = expectedMenu.taxCategories.taxCategory;
  const actualMenuTaxCategories = actualMenu.taxCategories.taxCategory;

  const expectedMenuGlobalModifiers = expectedMenu.globalModifiers;
  const actualMenuGlobalModifiers = actualMenu.globalModifiers;

  const expectedMenuSessions = expectedMenu.session.sessions;
  const actualMenuSessions = actualMenu.session.sessions;

  it('Should compare category array size', () => {
    expect(actualMenuCategories.length).to.be.equal(expectedMenuCategories.length);
  });

  it('should compare category objects', () => {
    expectedMenuCategories.forEach(element => {
      const expectedCategory = element;
      const actualCategory = actualMenuCategories.find(x => x.name === expectedCategory.name);

      expect(actualCategory).excludingEvery(['id', 'merchantId', 'categoryId', 'isAuxiliary', 'lineNo', 'buttonColor', 'products', 'taxCategoryId', 'toGoTaxCategoryId']).to.deep.equal(expectedCategory);

      expectedCategory.products.forEach(element => {
        const expectedProduct = element;
        const actualProduct = actualCategory.find(x => x.name === expectedProduct.name);

        expect(actualProduct).excludingEvery(['id', 'attributeSet', 'categoryId', 'lineNo', 'taxCategoryId', 'toGoTaxCategoryId']).to.deep.equal(expectedProduct);
      })
    });
  });

  it('Should compare attributeSet array size', () => {
    expect(actualMenuAttributeSets.length).to.be.equal(expectedMenuAttributeSets.length);
  });

  it('should compare attributeSet objects', () => {
    expectedMenuAttributeSets.forEach(element => {
      const expectedAttributeSet = element;
      const actualAttributeSet = actualMenuAttributeSets.find(x => x.name === expectedAttributeSet.name);

      expect(actualAttributeSet).excludingEvery(['id', 'merchantId', 'categoryId', 'isAuxiliary', 'lineNo']).to.deep.equal(expectedAttributeSet);
    });
  });

  it('Should compare taxCategory array size', () => {
    expect(actualMenuTaxCategories.length).to.be.equal(expectedMenuTaxCategories.length);
  });

  it('should compare taxCategory objects', () => {
    expectedMenuTaxCategories.forEach(element => {
      if (!element.deleted) { //Skip checking the deleted tax categories
        const expectedTaxCategory = element;
        const actualTaxCategory = actualMenuTaxCategories.find(x => x.name === expectedTaxCategory.name);

        expect(actualTaxCategory).excludingEvery(['id', 'taxCategoryId', 'validFrom', 'taxCustCategoryId', 'parentId', 'cascade', 'order']).to.deep.equal(expectedTaxCategory);
      }
    });
  });

  it('Should compare global modifier array size', () => {
    expect(actualMenuGlobalModifiers.length).to.be.equal(expectedMenuGlobalModifiers.length);
  });

  it('should compare global modifier objects', () => {
    expectedMenuGlobalModifiers.forEach(element => {
      const expectedGlobalModifier = element;
      const actualGlobalModifier = actualMenuGlobalModifiers.find(x => x.name === expectedGlobalModifier.name);

      expect(actualGlobalModifier).excludingEvery(['id', 'attributeValueId']).to.deep.equal(expectedGlobalModifier);
    });
  });

  it('should check session array size', () => {
    expect(actualMenuSessions.length).to.be.equal(expectedMenuSessions.length);
  });

  it('should compare session objects', () => {
    actualMenuSessions.forEach(element => {
      const actualSession = element;
      const expectedSession = expectedMenuSessions.find(x => x.name === actualSession.name);

      expect(actualSession).excludingEvery(['id','slots','categories']).to.deep.equal(expectedSession);

      expectedSession.slots.forEach(slot => {
        const expectedSlot = slot;
        const actualSlot = actualSession.slots.find(x => x.dayOfWeek === expectedSlot.dayOfWeek);
        
        expect(actualSlot).excludingEvery('id').to.deep.equal(expectedSlot);
      })
    });
  });


});